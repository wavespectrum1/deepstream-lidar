import open3d as o3d
import time
import os

def visualize_pcd_files(directory, hz=60):
    pcd_files = sorted([os.path.join(directory, f) for f in os.listdir(directory) if f.endswith('.pcd')])
    vis = o3d.visualization.VisualizerWithKeyCallback()
    vis.create_window()
    
    def update_visualization(vis):
        nonlocal index, pcd_files, hz
        if index < len(pcd_files):
            pcd = o3d.io.read_point_cloud(pcd_files[index])
            vis.clear_geometries()
            vis.add_geometry(pcd)
            vis.poll_events()
            vis.update_renderer()
            index += 1
        else:
            vis.destroy_window()
        return False
    
    index = 0
    vis.register_key_callback(ord(" "), update_visualization)
    
    while index < len(pcd_files):
        update_visualization(vis)
        time.sleep(1 / hz)

if __name__ == "__main__":
    visualize_pcd_files('./downsampled_pcd', hz=10)

