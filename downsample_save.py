import open3d as o3d
import numpy as np
import os

def aggregate_point_clouds(point_clouds):
    aggregated_points = np.vstack([pcd.points for pcd in point_clouds])
    aggregated_pcd = o3d.geometry.PointCloud()
    aggregated_pcd.points = o3d.utility.Vector3dVector(aggregated_points)

    if any([pcd.has_colors() for pcd in point_clouds]):
        aggregated_colors = np.vstack([pcd.colors for pcd in point_clouds])
        aggregated_pcd.colors = o3d.utility.Vector3dVector(aggregated_colors)

    if any([pcd.has_normals() for pcd in point_clouds]):
        aggregated_normals = np.vstack([pcd.normals for pcd in point_clouds])
        aggregated_pcd.normals = o3d.utility.Vector3dVector(aggregated_normals)

    return aggregated_pcd

def downsample_pcd_files(input_directory, output_directory, aggregation_factor):
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    total_files = len([name for name in os.listdir(input_directory) if os.path.isfile(os.path.join(input_directory, name))])
    aggregated_pcds = []
    count = 0

    for i in range(1, total_files + 1):
        file_path = f"{input_directory}/{i}.pcd"
        pcd = o3d.io.read_point_cloud(file_path)
        aggregated_pcds.append(pcd)

        if len(aggregated_pcds) == aggregation_factor:
            aggregated_pcd = aggregate_point_clouds(aggregated_pcds)
            output_path = f"{output_directory}/{count + 1}.pcd"
            o3d.io.write_point_cloud(output_path, aggregated_pcd)
            aggregated_pcds = []
            count += 1

    # Handle remaining point clouds if they exist
    if aggregated_pcds:
        aggregated_pcd = aggregate_point_clouds(aggregated_pcds)
        output_path = f"{output_directory}/{count + 1}.pcd"
        o3d.io.write_point_cloud(output_path, aggregated_pcd)

# Set the input and output directories
input_directory = "./pcd"
output_directory = "./downsampled_pcd"
aggregation_factor = 150  # Since 1500 Hz / 10 Hz = 150

downsample_pcd_files(input_directory, output_directory, aggregation_factor)

